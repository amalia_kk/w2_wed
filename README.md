# Bash script, Variables, Arguments, and Conditions


This will be a small demo on learning how to use the above.

By then we'll have a small script that uses these to imstall nginx on an ubuntu machine when given an IP address as an argument.

We'll talk about the difference betw. arguments and variables.

We'll also look into using conditions to help us run our script using control flow and handling errors.

### Script basics

Script is several bash commands written in a file that a bash interpreter can execute. This is useful to automate tasks and others.


### Running script

To run a script, it must first be executable. Check its permissions using 'll' or 'ls -l'. It should have 'x' to be executed. Add the permission 'execute' by using `chmod +x <file>`.

You might need to do this in a remote machine when moving or creating new files. You can run the file just by pointing to it or calling bash <file>.

```bash
# Pointing to the file
./bash_for_variables.sh
```
The dot represents 'here'. You could put the enter path to the file:
/Users/Amalia/Code/Week2//Wednesday/bash_for_variables.sh

 Also, running it remotely might be easier to call the file using a full bash command:

```bash
# To run the file remotely, call it using bash:
bash ./vas_bash.sh
```


### Escape notations

In bash and other languages, you have certain characters that behave/signal the important things other than the actual symbol. E.g. `""` are used to outline a string of characters.

How do you print out/echo out some `"`?
**You use what is called an escape character. In bash, this is the backslash (\)** 

```bash
#Example

echo "\$1"
echo "\" \" "

# Also single quotes protect double quotes and vice versa

echo "I'm a cool guy"
echo 'This is a double quotes """""""""""""'
```


### Argument in Scripts

Arguments are data that a function ncan take in and use internally.

```$ touch <argument>``` or ```$ mv <argument1> <argument2>```

In script, if you want to use arguments, they are defined with $#, where # is a number representing the numerical order in which the argument was passed to the script.

For example:
```bash
echo $1
echo $1 $2


# Interpolation of variables into a string

echo "this is argument 1: $1"
echo "this is argument 1: $2"
echo "this is argument 1: $3"
echo "this is argument 1: $4" " will you break?" 
```


Imagine you want to make a funtion or a script that takes in any number of arguments and does something.

You can use `$*` to represent all given arguments in a list.

To exmplain the above, we need loops. 

### Loops

A loop is a block of code thats runs for a given number of times. Good for repetitive tasks.

```bash

for x in [item item2 item3]
do
echo $x
done

```

In a loop, the program iterates over the iteratable objects (usually a list), substituting the x for each object in the list with each iteration (one per iteration).

### Conditions