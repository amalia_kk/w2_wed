## Bash script for testing Arguments and user input

# Escape characters example
echo "\$1"
echo "\" \" "

# Also single quotes protect double quotes and double quotes protect single quotes

echo "I'm a cool guy"

## An argument
#data that a funtion can take in and use internally
# E.g. $ touch <argument>
# or
# $ mv <argument1> <argument 2>

# In script if you want to use arguments, they're defined with $#, where # is a number
# For example

echo "\$1"
echo $1


# Interpolation of variable into a strings

echo "this is argument 1: $1" 
echo "this is argument 1: $2"
echo "this is argument 1: $3"
echo "this is argument 1: $4" " will you break?" 


# To call this script with four arguments, call the script and pass each argument...
# Syntax
#./var_basj.sh hi Filipe James Kofi Amalia

