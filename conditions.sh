# Conditions evaluate if something is true or false.
# This allows us to control flro- meaning our code will take different actions depending on the condition.


# Syntax
# if ((<condition>))
# then
   #block of code
# else
    # block of code
# fi


# Example:

echo "Input a number"
read number
if ((number < 5))
then 
    echo "Number is smaller than 5"
else
    echo "Number is larger than 5"
fi 