# Loops in bash

for x in item item2 banana bread milk butter
do
echo "you need to buy: $x"
done 

## Make a new list w your favourite movies and print it
for movie in Constantine Whiplash Fracture Devil_Wears_Prada
do
echo "$movie"
done




## Make a list of crazy ex landlords, use a loop to print then tell how many landlords there were
## Don't just hard code, use a variable to count i.e. initiate a counter.
## Ensure counter is outside of the loop

y=0
for landlord in Karen John Claire Barry
do
y=$(expr $y + 1)
echo "$landlord"
done
echo $y 


## Make a new list w your fav restaurants and print it out using a loop and in this format:
# > 1 - item
# > 2 - item2
# > 3 - item3

counter=1
for restaurants in "Yo! Sushi" "Jade Palace" "Well Springs"
do
echo "$counter - $restaurant"
counter=$(expr $counter + 1)
done



### Using arguments w for loops
# $* means any number of arguments passed to script

for arg in $*
do 
echo "This was one of your arguments: $arg"
done 
